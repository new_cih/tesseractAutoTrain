package org.newcih.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.List;

/**
 * Tesseract的S
 *
 * @author newcih
 * @version 2018-02-05
 */
@Service
public class TesseractService {
    /**
     * 存放字体数据的路径
     */
    @Value("${TESS_DATA}")
    private String tessData;


}
