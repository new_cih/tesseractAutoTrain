package org.newcih.tesstrain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"org.newcih.web", "org.newcih.service"})
@SpringBootApplication
public class TesstrainApplication {


    public static void main(String[] args) {

        SpringApplication.run(TesstrainApplication.class, args);

    }
}
