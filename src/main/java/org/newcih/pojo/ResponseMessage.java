package org.newcih.pojo;

public class ResponseMessage {

    private int    code;
    private String message;
    private Object value;

    /**
     * 基本状态码
     */
    public static final int OK    = 200;
    public static final int ERROR = 400;

    /**
     * 识别接口状态码
     */
    public static final int TESS_OK    = 3000;
    public static final int TESS_ERROR = 3001;

    /**
     * 训练接口状态码
     */
    public static final int TRAIN_OK    = 4000;
    public static final int TRAIN_ERROR = 4001;

    public ResponseMessage() {

    }

    public ResponseMessage(int code, String message, String value) {
        this.code = code;
        this.message = message;
        this.value = value;
    }

    /**
     * 正确对象
     *
     * @param value
     * @return
     */
    public static ResponseMessage ok(Object value) {
        return ok(OK, value);
    }

    public static ResponseMessage ok() {
        return ok(OK, null);
    }

    public static ResponseMessage ok(int code, Object value) {
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setValue(value);
        responseMessage.setCode(code);
        return responseMessage;
    }

    /**
     * 错误对象
     *
     * @param message
     * @return
     */
    public static ResponseMessage error(String message) {
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setCode(ERROR);
        responseMessage.setMessage(message);
        return responseMessage;
    }

    public static ResponseMessage error(int code, String message) {
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setCode(code);
        responseMessage.setMessage(message);
        return responseMessage;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
